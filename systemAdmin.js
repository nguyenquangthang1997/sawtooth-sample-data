const axios = require('axios');
var random_name = require('node-random-name');
axios.defaults.baseURL = 'http://localhost:8000';

// axios.defaults.headers.common['Authorization']
function wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
        end = new Date().getTime();
    }
}


async function main() {
    var system_user_name = random_name({first: true});
    try {
        var createSystemAdmin = await axios.post('create_systemadmin', {

            "name": random_name(),
            "username": system_user_name,
            "password": "1",
            "number_phone": "1221212",
            "address": "HN",
            "org_id": ""
        });
        console.log('systemadmin done')

    } catch (e) {
        console.log('systemadmin')
        console.log(e.response.data)
    }
    wait(2000)

    try {

        var loginAdmin = await axios.post('authentication', {
            "username": system_user_name,
            "password": "1",
        });
        console.log('login systemadmin done')

        var system_authenication = loginAdmin.data.authorization

    } catch (e) {
        console.log(e.response.data)

    }


    try {
        var org_name = random_name({first: true});
        var createOrg = await axios.post('systemAdmin/org', {
            "org_name": random_name(),
            "username": org_name,
            "role": 0,
            "password": "1",
            "address": "noi",
            "phoneNumber": "0974755580"
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + system_authenication
                }
        });
        console.log('create org done')

    } catch (e) {
        console.log("createOrg")
        console.log(e.response.data)

    }

    wait(2000)
    try {
        var loginOrg = await axios.post('systemAdmin/login', {
            "username": org_name,
            "password": "1"
        });
        var org_authenication = loginOrg.data.authorization
        console.log('login org done')

    } catch (e) {
        console.log('login org')
        console.log(e.response.data)

    }

    try {

        var createLand = await axios.post('land', {
            "acreage": "100m2",
            "status": 0,
            "longitude": 21,
            "latitude": 32
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + org_authenication
                }
        });
        console.log('create land1 done')
    } catch (e) {
        console.log('createLand1');
        console.log(e.response.data)
    }
    console.log('createLand1');

    try {
        var createLand = await axios.post('land', {
            "acreage": "100m2",
            "status": 0,
            "longitude": 21,
            "latitude": 32
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + org_authenication
                }
        });
    } catch (e) {
        console.log('createLand2');
        console.log(e.response.data)
    }
    console.log('createLand2');
    try {
        var createLand = await axios.post('land', {
            "acreage": "100m2",
            "status": 0,
            "longitude": 21,
            "latitude": 32
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + org_authenication
                }
        });
        landId = createLand.data.landId
    } catch (e) {
        console.log('createLand3');
        console.log(e.response.data)
    }
    console.log('createLand3');
    try {
        var user_name = random_name({first: true});

        var createUser = await axios.post('orgs', {
            "name": random_name(),
            "username": user_name,
            "password": "1",
            "number_phone": "thaont",
            "address": "HN",
            "role": 0
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + org_authenication
                }
        });
    } catch (e) {
        console.log('createUser1');
        console.log(e.response.data)
    }
    console.log('createUser1');
    try {
        var user_name = random_name({first: true});
        var createUser = await axios.post('orgs', {
            "name": random_name(),
            "username": user_name,
            "password": "1",
            "number_phone": "thaont",
            "address": "HN",
            "role": 0
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + org_authenication
                }
        });
    } catch (e) {
        console.log('createUser2');
        console.log(e.response.data)
    }
    console.log('createUser2');
    try {
        var user_name = random_name({first: true});
        var createUser = await axios.post('orgs', {
            "name": random_name(),
            "username": user_name,
            "password": "1",
            "number_phone": "thaont",
            "address": "HN",
            "role": 0
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + org_authenication
                }
        });
        var userId = createUser.data.userId;
    } catch (e) {
        console.log('createUser3');
        console.log(e.response.data)
    }
    console.log('createUser3');
    wait(4000)
    try {
        var addLandToUser = await axios.post('orgs/addLandToUser', {
            "user_id": userId,
            "farm_id": landId
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + org_authenication
                }
        })
        console.log(addLandToUser.data)
    } catch (e) {
        console.log('addlandtouser');
        console.log(e.response.data)
    }
    console.log('addlandtouser');


    try {
        var loginUser = await axios.post('authentication', {
            "username": user_name,
            "password": "1",
        })
    } catch (e) {
        console.log('login user');
        console.log(e.response.data)
    }
    console.log('login user');
    wait(2000)
    var user_authenication = loginUser.data.authorization
    try {
        var createSeason = await axios.post('land/seasons', {
            "landId": landId,
            "productName": "Dua Luoi"
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + user_authenication
                }
        })
        var seasonId = createSeason.data.uid
        console.log(createSeason.data)
    } catch (e) {
        console.log('createSeason');
        console.log(e.response.data)
    }
    console.log('createSeason');

    wait(2000)

    try {
        console.log(seasonId)
        var createAction = await axios.post('action', {
            "seasonId": seasonId,
            "type": "phun thuoc sau",
            "description": "2 lit thuoc diet co",
            "note": "dung lau"
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + user_authenication
                }
        })
    } catch (e) {
        console.log('createAction1');
        console.log(e.response.data)
    }
    console.log('createAction1');

    try {
        var createAction = await axios.post('action', {
            "seasonId": seasonId,
            "type": "bon phan",
            "description": "2 kg phan Kali",
            "note": "bon thuc"
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + user_authenication
                }
        })
    } catch (e) {
        console.log('createAction2');
        console.log(e.response.data)
    }
    console.log('createAction2');
    try {
        var createAction = await axios.post('action', {
            "seasonId": seasonId,
            "type": "phun thuoc sau",
            "description": "2 lit xang",
            "note": "dot cay"
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + user_authenication
                }
        })
    } catch (e) {
        console.log('createAction3');
        console.log(e.response.data)
    }
    console.log('createAction3');
    try {
        var requestCreate = await axios.post('users/generate', {
            "seasonId": seasonId,
            "number": 20
        }, {
            headers:
                {
                    Authorization: 'Bearer ' + user_authenication
                }
        });
    } catch (e) {
        console.log('createrequest');
        console.log(e.response.data)
    }
    console.log('createrequest');
    wait(2000)


    try {
        var getRequest = await axios.get('orgs/requestHandle', {
            headers:
                {
                    Authorization: 'Bearer ' + org_authenication
                }
        });
        requestId = getRequest.data[0].requestId;
    } catch (e) {
        console.log('getrequest');
        console.log(e.response.data)
    }

    wait(2000)
    console.log('getrequest');


    try {
        var approveRequest = await axios.post('orgs/requestHandle',
            {
                "requestId": requestId
            }, {
                headers:
                    {
                        Authorization: 'Bearer ' + org_authenication
                    }
            }
        );
    } catch (e) {
        console.log('aprrove');
        console.log(e.response.data)
    }
    console.log('aproverequest');

    wait(3000)

    try {
        var productIdList = await axios.get('users/generate', {
                data: {
                    "seasonId": seasonId
                },
                headers:
                    {
                        Authorization: 'Bearer ' + user_authenication
                    }
            }
        );
    } catch (e) {
        console.log('getproductList');
        console.log(e.response.data)
    }
    console.log('getproductList');

    wait(2000)
    try {
        productId = productIdList.data[3]
        var createProduct = await axios.post('product',
            {
                "productId": productId,
                "status": 0,
                "seasonId": seasonId,
                "description": "rau den",
                "temperature": "22",
                "humidity": "90",
                "latitude": 2848593,
                "longitude": 3945064,
                "guide": "nau canh"
            }, {
                headers:
                    {
                        Authorization: 'Bearer ' + user_authenication
                    }
            }
        )
    } catch (e) {
        console.log('createProduct1');
        console.log(e.response.data)
    }
    console.log('createproduct1');
    try {
        productId = productIdList.data[7]
        var createProduct = await axios.post('product',
            {
                "productId": productId,
                "status": 0,
                "seasonId": seasonId,
                "description": "rau muong",
                "temperature": "22",
                "humidity": "90",
                "latitude": 2848593,
                "longitude": 3945064,
                "guide": "nau canh"
            }, {
                headers:
                    {
                        Authorization: 'Bearer ' + user_authenication
                    }
            }
        )
    } catch (e) {
        console.log('createProduct2');
        console.log(e.response.data)
    }
    console.log('createproduct2');

    try {
        productId = productIdList.data[10]
        var createProduct = await axios.post('product',
            {
                "productId": productId,
                "status": 0,
                "seasonId": seasonId,
                "description": "rau mumg toi",
                "temperature": "22",
                "humidity": "90",
                "latitude": 2848593,
                "longitude": 3945064,
                "guide": "nau canh"
            }, {
                headers:
                    {
                        Authorization: 'Bearer ' + user_authenication
                    }
            }
        )
    } catch (e) {
        console.log('createProduct3');
        console.log(e.response.data)
    }
    console.log('createproduct3');

}

main()

